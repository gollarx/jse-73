package ru.t1.shipilov.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.shipilov.tm.model.AbstractModel;

@NoRepositoryBean
public interface IModelRepository<M extends AbstractModel> extends JpaRepository<M, String> {
}
