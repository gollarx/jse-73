package ru.t1.shipilov.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.model.SessionDTO;

public interface SessionConstant {

    int INIT_COUNT_SESSIONS = 5;

    @Nullable
    SessionDTO NULLABLE_SESSION = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_SESSION_ID = null;

    @NotNull
    String EMPTY_SESSION_ID = "";

    @Nullable
    Integer NULLABLE_INDEX = null;

}