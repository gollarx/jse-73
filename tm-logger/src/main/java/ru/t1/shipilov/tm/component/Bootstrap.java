package ru.t1.shipilov.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.listener.LoggerListener;
import ru.t1.shipilov.tm.service.ReceiverService;

@NoArgsConstructor
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    @NotNull
    @Autowired
    private LoggerListener loggerListener;

    @SneakyThrows
    public void start() {
        receiverService.receive(loggerListener);
    }

}

